# This file is gathering images from a usb camera and storing them for tensorflow usage.
import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F

import matplotlib.pyplot as plt
import numpy as np
import image2angle.test_common as tc

if __name__ == '__main__':
    PATH = './cifar_net.pth'

    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                           download=True, transform=transform)
    testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                             shuffle=False, num_workers=2)

    dataiter = iter(testloader)
    images, labels = dataiter.next()


    classes = ('plane', 'car', 'bird', 'cat',
               'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
    # print images


    net = tc.Net()
    net.load_state_dict(torch.load(PATH))

    outputs = net(images)
    print(outputs)
    _, predicted = torch.max(outputs, 1)

    print(predicted)
    print(labels)

    with torch.no_grad():
        correct = 0
        total = 0
        classes_correct = list(0. for i in range(10))
        classes_total = list(0. for i in range(10))
        print(classes_correct)
        for data in testloader:
            images, label = data
            outputs = net(images)
            _, predicted = torch.max(outputs, 1)
            total += label.size(0)
            correct += (label == predicted).sum().item()
            predicted.size()
            for i in range(predicted.size(0)):
                if label[i] == predicted[i]:
                    classes_correct[label[i]] += 1
                classes_total[label[i]] += 1



    print('Total: ' + str(total))
    print('Correct: ' + str(correct/total*100) + ' %')

    for i in range(10):
        print('Accuracy of '+ classes[i] + ': ' + str(classes_correct[i]/classes_total[i]*100) + '%')





