import time
import serial


if __name__ == '__main__':

    with serial.Serial(port='COM3', baudrate=500000, timeout=0.01) as ser:  # open serial port
        print(ser.name)         # check which port was really used
        ser.write(b'xy')     # write a string
        ser.write(b'r0')
        time.sleep(1)

        ser.write(b'x')
        ser.write(b'r-1')
        time.sleep(0.2)
        ser.write(b'r0')
        time.sleep(1)

        kpp = 14
        kpi = 0.2
        kpd = 250

        ser.write(b'kp' + f'{kpp:.2f}'.encode())
        ser.write(b'kp' + f'{kpi:.2f}'.encode())