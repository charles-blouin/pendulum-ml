import time
import serial
import math
import numpy as np

# This file is for the ping script.

if __name__ == '__main__':

    with serial.Serial(port='COM3', baudrate=500000, timeout=0.5, dsrdtr=True) as ser:  # open serial port
        print(ser.name)         # check which port was really used
        ser.write(b'xy')     # write a string
        ser.write(b'o');
        ser.write(b'r0')
        time.sleep(0.5)

        iterations = 100
        returned_ping = 0
        start_time = time.time()
        return_time = np.zeros([iterations])
        return_val = []

        for i in range(iterations):
            # cmd = math.sin(time.time()*2*math.pi) * 10

            # ser.write(b'r' + bytes(f"{cmd:.1f}", 'utf-8'))
            # time.sleep(0.02)
            write_time = time.time_ns()
            ser.write(b'P')
            input = ser.readline()
            return_val.append(input)
            return_time[i] = (time.time_ns() - write_time)  / (10 ** 9)


        print(f"Elapsed time: {time.time() - start_time}")
        print(f"Maximum: {max(return_time)}")
        print(return_time)
        print(return_val)
        print(f"Length of return_val = {len(return_val)}")

        ser.write(b'n')  # write a string


# The return is extremely fast with a single byte return, a string ("ping") (27 ms for 100 msg),
# or a float (54 ms for 100 msg). Int is (27 ms for 100 msg). Float to string conversion is slower.
# Elapsed time: 0.03210282325744629
# Maximum: 0.001301
# [0.0011025 0.0009345 0.001     0.        0.        0.        0.
#  0.000966  0.0010992 0.0001794 0.0002279 0.000653  0.0001792 0.0002258
#  0.0001794 0.0001793 0.001111  0.        0.        0.        0.
#  0.0010326 0.0010336 0.0001783 0.0002279 0.0001776 0.0001793 0.0001793
#  0.0001792 0.000235  0.0005773 0.        0.        0.        0.
#  0.000972  0.        0.        0.0010939 0.        0.        0.001002
#  0.        0.        0.0010579 0.0008743 0.        0.        0.
#  0.0009789 0.        0.        0.        0.        0.0010894 0.0001793
#  0.0002247 0.0001777 0.0001793 0.0001795 0.000179  0.000235  0.000643
#  0.0009343 0.        0.        0.        0.        0.0009821 0.0010832
#  0.0001792 0.0002502 0.0001791 0.0001794 0.0001794 0.0001793 0.0002108
#  0.0005773 0.        0.        0.        0.0007649 0.        0.
#  0.        0.001301  0.0001792 0.0002274 0.0001775 0.0001793 0.0001793
#  0.0001793 0.0002349 0.0005772 0.        0.        0.        0.
#  0.0010009 0.0010645]
# [b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n', b'P\r\n']
# Length of return_val = 100
