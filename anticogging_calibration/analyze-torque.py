import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq


def fit_sine(x, y):
    order = 2
    guess_mean = [np.mean(y), 0]
    guess_std = [3 * np.std(y) / (2 ** 0.5) / (2 ** 0.5), 0]
    guess_phase = [0, 0]
    guess_freq = [0.0008, 0]
    guess_amp = [abs(np.max(y) - guess_mean[0]), 0]

    est_amp = [0] * 3
    est_freq = [0]
    est_phase = [0] * 3
    est_mean = [0]

    # we'll use this to plot our first estimate. This might already be good enough for you
    data_first_guess = guess_std[0] * np.sin(guess_freq[0] *x + guess_phase[0]) + guess_mean[0]

    # Define the function to optimize, in this case, we want to minimize the difference
    # between the actual data and our "guessed" parameters
    optimize_func = lambda h: h[0] * np.sin(h[1] * x + h[2]) + h[3] - y
    est_amp[0], est_freq[0], est_phase[0], est_mean[0] = leastsq(optimize_func, [guess_amp[0], guess_freq[0], guess_phase[0], guess_mean[0]])[0]
    leastsq(optimize_func, [guess_amp[0], guess_freq[0], guess_phase[0], guess_mean[0]])[0]

    def optimize_func2(h):
        res = h[0] * np.sin(h[1] * x + h[2]) + h[3] - y
        res += h[4] * np.sin(2* h[1] * x + h[5])
        res += h[6] * np.sin(3 * h[1] * x + h[7])
        return res
    # optimize_func2 = lambda h: h[0] * np.sin(h[1] * x + h[2]) + h[3] - y \
    #                          + h[4] * np.sin(2* h[1] * x + h[5])
    est_amp[0], est_freq[0], est_phase[0], est_mean[0], est_amp[1], est_phase[1], est_amp[2], est_phase[2] = leastsq(optimize_func2, \
                                                                 [est_amp[0], est_freq[0], est_phase[0], est_mean[0], \
                                                                  0, 0, 0, 0])[0]


    # recreate the fitted curve using the optimized parameters

    fine_t = np.arange(0, max(x), max(x)/1000)
    data_fit = est_amp[0] * np.sin(est_freq[0] * fine_t + est_phase[0]) + est_mean[0]
    data_fit2 = est_amp[0] * np.sin(est_freq[0] * fine_t + est_phase[0]) + est_mean[0] + \
                est_amp[1] * np.sin(2 * est_freq[0] * fine_t + est_phase[1]) + \
                est_amp[2] * np.sin(3 * est_freq[0] * fine_t + est_phase[2])
    print(est_amp[1])
    print(est_phase[1])
    plt.plot(x, y)
    plt.plot(x, data_first_guess, label='first guess')
    plt.plot(fine_t, data_fit, label='after fitting')
    plt.plot(fine_t, data_fit2, label='after fitting second order')
    plt.legend()
    plt.show()

if __name__ == '__main__':
    with open('test_data/60-0.25v1.npy', 'rb') as f:
        angleArray = np.load(f)
        uArray = np.load(f)
    with open('test_data/60-0.25v2.npy', 'rb') as f:
        angleArray2 = np.load(f)
        uArray2 = np.load(f)

    for i in range(len(angleArray2)-1  ):
        print(angleArray2[i] - angleArray2[i+1])
    fit_sine(angleArray2[50:450], uArray2[50:450])

# plt.plot(angleArray, uArray, angleArray2, uArray2)
# plt.xlabel('Angle')
# plt.ylabel('Effort')
# plt.show()

