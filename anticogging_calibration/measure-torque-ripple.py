import time
import serial
import matplotlib.pyplot as plt
import numpy as np

class Recorder:
    def __init__(self, ser):
        self.angleArray = []
        self.uArray = []
        self.timeArray = []
        self.startTime = time.time()
        self.ser = ser
        self.ser.reset_input_buffer()

    def record(self, duration):
        nt = time.time()
        while (time.time() - nt) < duration:
                input = self.ser.readline()
                print(input)
                input_string = input.decode()
                if len(input_string) > 0 and input_string[0] == 'u':
                    self.timeArray.append(time.time() - self.startTime)
                    index_u = input_string.index('u')
                    index_a = input_string.index('a')
                    u = int(input_string[1:index_a])
                    self.uArray.append(u)
                    angle = int(input_string[index_a+1:])
                    self.angleArray.append(angle)

    def recordAverage(self, nb_points):
        sum_angle = 0
        sum_u = 0
        i = 0
        while i < nb_points:
            try:
                input = ser.readline()
                input_string = input.decode()
                if len(input_string) > 0 and input_string.rfind('u') == 0:
                    index_u = input_string.index('u')
                    index_a = input_string.index('a')
                    u = int(input_string[1:index_a])
                    angle = int(input_string[index_a+1:])
                    sum_u += u
                    sum_angle += angle
                    i += 1
            except ValueError:
                print("Packet dropped")
        self.uArray.append(sum_u/nb_points)
        self.angleArray.append(sum_angle/nb_points)
        self.timeArray.append(time.time() - self.startTime)

if __name__ == '__main__':
# This script will perform a rotation, stopping every step_size. The settling time is necessary to obtain a steady
# state measurement. Once the test is done, the result will be saved in a .npy file. This file needs to be loaded in the
# interpolate_360 script to generate the calibration table.
    with serial.Serial(port='COM3',baudrate=500000, timeout=0.01) as ser:  # open serial port
        start_angle = -5
        end_angle = 370
        step_size = 0.25
        kp_p = 120
        kp_i = 0
        kp_d = 0
        time.sleep(0.3)
        ser.write(b'kp' + f'{kp_p:.2f}'.encode())
        ser.write(b'ki' + f'{kp_i:.2f}'.encode())
        ser.write(b'kd' + f'{kp_d:.2f}'.encode())
        ser.write(b'q')
        time.sleep(0.3)
        print(ser.read_all())
        print(ser.name)         # check which port was really used
        ser.write(b'xy')     # write a string
        ser.write(b'u')  # Print effort and angle
        ser.write(b'r' + str(start_angle).encode('ascii'))
        time.sleep(1)
        print(ser.read_all())


        recorder = Recorder(ser)
        ser.reset_input_buffer()
        for i in np.arange(start_angle, end_angle, step_size):
            ser.write(b'r' + f'{i:.1f}'.encode())
            time.sleep(0.1)
            ser.reset_input_buffer()
            recorder.recordAverage(20)
            print(f'{i:.2f}')
        print("Backward")
        for i in np.arange(end_angle, start_angle, -step_size):
            ser.write(b'r' + f'{i:.1f}'.encode())
            time.sleep(0.1)
            ser.reset_input_buffer()
            recorder.recordAverage(20)
            print(i)

        ser.write(b'r0')
        time.sleep(0.5)
        ser.write(b'n')
        ser.close()             # close port

        # plt.plot(recorder.timeArray, recorder.angleArray, recorder.timeArray, recorder.uArray)
        plt.plot(recorder.angleArray, recorder.uArray)
        plt.xlabel('Angle')
        plt.ylabel('Effort')


        with open('test.npy', 'wb') as f:
            np.save(f, np.array(recorder.angleArray))
            np.save(f, np.array(recorder.uArray))
        with open('test.npy', 'rb') as f:
            a = np.load(f)
            b = np.load(f)
        print(a, b)

        plt.show()
