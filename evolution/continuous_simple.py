from __future__ import print_function
import torch
import gym

import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class Net(nn.Module):

    def __init__(self, nb_inputs, nb_outputs):
        super(Net, self).__init__()
        # an affine operation: y = Wx + b
        self.nb_inputs = nb_inputs
        self.nb_outputs = nb_outputs
        self.fc1 = nn.Linear(self.nb_inputs, 4)
        self.fc2 = nn.Linear(4, 2)
        self.fc3 = nn.Linear(2, self.nb_outputs)

    def forward(self, x):
        x = F.tanh(self.fc1(x))
        x = F.tanh(self.fc2(x))
        x = F.tanh(self.fc3(x))
        return x


# genetic and evolutionary algorithm

mutation_chance = 0.01
generations = 30
life = 1
top_couples = 2 # How many couples reproduce
number_of_kids = 10
pop_size = (number_of_kids+2) * top_couples # We also keep the parents (top performing) for next gen.
# each weight is a gene.

def select_action(tensor_in):
    values, indices = torch.max(tensor_in, 1)
    if (tensor_in[0,0].item()) >= 0.5:
        action = 1
    else:
        action = 0
    return(indices.item())

def eval_agent(env, net):
    state = env.reset()
    num_steps = 0
    done, performance = 0, 0
    while not done:
        state_tensor = torch.tensor([state], dtype=torch.float)
        action = net(state_tensor).detach().numpy()[0]*3
        # print(action)
        state, reward, done, _ = env.step(action)
        performance += reward
        num_steps += 1
    return performance, num_steps

def enjoy_env(env, net):
    state = env.reset()
    num_steps = 0
    done, performance = 0, 0
    while not done:
        state_tensor = torch.tensor([state], dtype=torch.float)
        action = net(state_tensor).detach().numpy()[0]*3
        # print(action)
        state, reward, done, _ = env.step(action)
        env.render()
        performance += reward
        num_steps += 1
    return performance, num_steps

def reproduce(father, mother, mutation_rate):
    # merge two neural nets
    # torch.randint(low=0, high=1)
    kid = Net(father.nb_inputs, father.nb_outputs)
    with torch.no_grad():
        kid_sd = kid.state_dict()
        for (name_f, param_f), (name_m, param_m), (name_k, param_k) in \
                zip(father.named_parameters(), mother.named_parameters(), kid.named_parameters()):
            # print(name_f, param_f)
            # print(name_m, param_m)
            # The kid inherits parameters from both parents
            mask_parent = torch.rand(param_f.size())
            mask_parent = torch.ge(mask_parent, 0.5)
            kid_sd[name_k].copy_(mask_parent * param_f + ~mask_parent * param_m)

            # The kid also has random mutations.
            mutation_matrix = torch.rand(param_f.size())
            mask_mutation = torch.le(mutation_matrix, mutation_rate)
            # We use mutation_matrix as a source of random elements
            kid_sd[name_k].masked_scatter_(mask_mutation, mutation_matrix)
    return kid
if __name__ == '__main__':
    env = gym.make("BipedalWalker-v3")
    env.reset()
    obs_size = env.observation_space.high.size
    action_size = env.action_space.high.size
    parents = [Net(obs_size, action_size) for i in range(pop_size)]
    performance = [0] * pop_size
    a = torch.ones(1, 4)
    num_steps = 0 # Running total of number of steps


    for generation_id in range(generations):
        print('---- Generation ' , generation_id)
        performance = [0] * pop_size
        for i in range(pop_size):
            for j in range(life):
                p, ns = eval_agent(env, parents[i])
                performance[i] += p
                num_steps += ns
        hp = np.argsort(performance)
        generation_average = np.mean(performance)
        # top_couples_average = np.mean(performance[hp[-top_couples*2:]])

        print('generation_average: ', generation_average, performance)
        print('Number of steps: ', num_steps)
        # print('top_couples_average: ', top_couples_average, np.mean(performance[hp[-top_couples*2:]]))
        # nets[hp[-i:]].item()
        top_performing_parents = [parents[i] for i in hp[-top_couples*2:]]


        # Reproduce and create next generation.
        for couple_id in range(top_couples):
            for kid_id in range(number_of_kids+2):
                # Copy the parents
                if kid_id == 0 or kid_id == 1:
                    parents[couple_id * number_of_kids + kid_id] = top_performing_parents[couple_id + kid_id]
                # Copy the childen
                else:
                    child = reproduce(top_performing_parents[couple_id*2], top_performing_parents[couple_id*2+1], mutation_chance)
                    parents[couple_id*number_of_kids + kid_id] = child


    # np.argmax()
    input("Press Enter to continue...")
    enjoy_env(env, top_performing_parents[0])
    enjoy_env(env, top_performing_parents[1])
    enjoy_env(env, top_performing_parents[2])
    enjoy_env(env, top_performing_parents[3])