import time
import serial
import math
import numpy as np

if __name__ == '__main__':

    import win32api, win32process, win32con

    pid = win32api.GetCurrentProcessId()
    handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)
    win32process.SetPriorityClass(handle, win32process.HIGH_PRIORITY_CLASS)

    with serial.Serial(port='COM3', baudrate=500000, timeout=0.001, dsrdtr=True) as ser:  # open serial port
        beginning = time.time()

        ser.write(b'y')
        ser.write(b't')
        for i in range(500):
            start = time.time_ns()
            # ser.write(b'r10')
            ser.write(b'P')     # write a string

            input = ser.readline()
            read_time = (time.time_ns()-start)/ (10 ** 9)

            if read_time > 0.01:
                print(f"{input} time:  {read_time}")
                print("#################################")

            time.sleep(0.01)
        ser.write(b'r0')
        ser.write(b'n')

        print((beginning - time.time())/500)