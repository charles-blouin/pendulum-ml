import numpy as np
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch

def diff_central(x, y):
    x0 = x[:-2]
    x1 = x[1:-1]
    x2 = x[2:]
    y0 = y[:-2]
    y1 = y[1:-1]
    y2 = y[2:]
    f = (x2 - x1)/(x2 - x0)
    return (1-f)*(y2 - y1)/(x2 - x1) + f*(y1 - y0)/(x1 - x0)

class Net(nn.Module):
    def __init__(self, features = 2):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(features, 20)
        self.fc2 = nn.Linear(20, 20)
        self.fc3 = nn.Linear(20, 1)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


net = Net()

if __name__ == '__main__':
    data = np.genfromtxt('log-02-27-092343.txt', delimiter=',')
    PATH = './network_01.pth'
    PATH_ONNX = './network_01.onnx'

    sf = 1 # Degrees to turns per seconds
    time = data[:, 0] * sf
    freq = data[:,1] * sf  # Time between frames
    ping = data[:, 2] * sf
    cmd = data[:, 3] * sf
    rot = data[:, 4] * sf
    rotvel = data[:, 5] * sf

    estimated_vel = diff_central(time, rot)
    estimated_acc = diff_central(time[1:-1], estimated_vel)

    time = time[2:-2]
    freq = freq[2:-2]  # Time between frames
    ping = ping[2:-2]
    cmd = cmd[2:-2]
    rot = rot[2:-2]
    rotvel = rotvel[2:-2]
    estimated_vel = estimated_vel[1:-1]

###### Define inputs and outputs. Format is [N, B], where n is the number of trainning example.
    # b is the number of features.

    cmd_norm = 10
    rotvel_norm = 1500
    acc_norm = 200000
    cmd_in = cmd.reshape((-1, 1))/ cmd_norm # Acceleration is not defined outside [-2, 2]
    rotvel_in = estimated_vel.reshape((-1, 1))/rotvel_norm
    inputs = np.concatenate((cmd_in, rotvel_in), axis=1)


    # Stack of previous inputs. N is number of sample
    stack = 0 # 0 means no past value is used
    N = estimated_acc.shape[0] - stack
    B = 2 * (1+stack) # 2 for velocity and cmd
    inputs = np.zeros((N, B))
    for n in range(N):
        for b in range(1+stack):
            inputs[n, b] = cmd_in[n+b, 0]
            inputs[n, b + stack + 1] = rotvel_in[n+b, 0]

    if stack:
        target = np.reshape(estimated_acc[0: -stack]/acc_norm, (-1, 1))
    else:
        target = np.reshape(estimated_acc / acc_norm, (-1, 1))
    print(inputs)

    # Define NN
    net = Net(B)
    criterion = nn.MSELoss()
    optimizer = optim.SGD(net.parameters(), lr=0.01, momentum=0.1)

    inputs = torch.tensor(inputs, dtype=torch.float)
    target = torch.tensor(target, dtype=torch.float)
    #print(inputs)
    # print(net(inputs))
    # print(target)

    for e in range(1000):
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, target)
        loss.backward()
        optimizer.step()

        if e % 100 == 0:
            print(f'Loss: {loss.item()}')

    torch.save(net.state_dict(), PATH)

    dummy_input = torch.tensor([[1, 1]], dtype=torch.float)
    input_names = ["cmd"] + ["rotvel"]
    torch.onnx.export(net, dummy_input, PATH_ONNX, verbose=True, input_names=input_names,
                      output_names=["torque"])
    print(net(dummy_input))

    accel_nn = outputs.detach().numpy()*acc_norm

    plt.plot(time, rot, 'o', label='Rot')
    plt.plot(time, cmd*100, 'o', label='Command * 100')
    # plt.plot(time, rotvel, label='Vel. from firmware')
    plt.plot(time, estimated_vel, 'o', label='Vel. Estimated')
    plt.plot(time, estimated_acc/10, 'o', label='Acc. Estimated/10')
    if stack:
        plt.plot(time[0:-stack], accel_nn/10, label='NN acceleration')
    else:
        plt.plot(time, accel_nn/10, label='NN acceleration')
    plt.legend()
    plt.show()