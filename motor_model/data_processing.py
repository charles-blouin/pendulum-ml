import numpy as np
import matplotlib.pyplot as plt


def diff_central(x, y):
    x0 = x[:-2]
    x1 = x[1:-1]
    x2 = x[2:]
    y0 = y[:-2]
    y1 = y[1:-1]
    y2 = y[2:]
    f = (x2 - x1)/(x2 - x0)
    return (1-f)*(y2 - y1)/(x2 - x1) + f*(y1 - y0)/(x1 - x0)


def diff_forward(x, y):
    x0 = x[:-2]
    x1 = x[1:-1]
    x2 = x[2:]
    y0 = y[:-2]
    y1 = y[1:-1]
    y2 = y[2:]
    return (y1 - y0)/(x1 - x0)

def generate_data_from_time_series(filename, previous_ns=2, plot=True):
    data = np.genfromtxt(filename, delimiter=',')
    PATH = './network_01.pth'
    PATH_ONNX = './network_01.onnx'

    sf = 1  # Degrees to turns per seconds
    time = data[:, 0] * sf
    freq = data[:, 1] * sf  # Time between frames
    ping = data[:, 2] * sf
    cmd = data[:, 3] * sf
    rot = data[:, 4] * sf
    rotvel = data[:, 5] * sf

    estimated_vel = diff_forward(time, rot)
    estimated_acc = diff_forward(time[1:-1], estimated_vel)

    time = np.asarray(time[2:-2])
    freq =np.asarray(freq[2:-2])  # Time between frames
    ping = np.asarray(ping[2:-2])
    cmd = np.asarray(cmd[2:-2])
    rot = np.asarray(rot[2:-2])
    rotvel = np.asarray(rotvel[2:-2])
    estimated_vel = np.asarray(estimated_vel[1:-1])

    length = len(estimated_vel)
    N = length - 1 - previous_ns # -1 for the acceleration prediction, -previous_ns for the start of the array.

    ###### Define inputs and outputs. Format is [N, B], where N is the number of training example.
    # B is the number of features.
    # [cmd_n, cmd_n-1, ]
    start_index = previous_ns
    end_index = -1
    inputs = np.concatenate((np.transpose([cmd[start_index:end_index]]), np.transpose([rotvel[start_index:end_index]])), 1)
    start_index -= 1
    end_index -= 1
    while start_index >= 0 :
        inputs = np.concatenate((inputs, np.transpose([cmd[start_index:end_index]]), np.transpose([rotvel[start_index:end_index]])),
                                1)
        start_index -= 1
        end_index -= 1

    outputs = [(estimated_acc[previous_ns:-1] + estimated_acc[previous_ns+1:])/2]
    outputs = np.transpose(outputs)
    if plot:
        plt.plot(time, rot, label='Rot')
        plt.plot(time, cmd * 1000, label='Command * 100')
        plt.plot(time, rotvel, label='Vel. from firmware')
        plt.plot(time, estimated_vel, label='Vel. Estimated')
        plt.plot(time, estimated_acc / 10, label='Acc. Estimated/10')
        plt.legend()
        plt.show()


    return (inputs, outputs)


# This code takes the files with test data and outputs a single file with the inputs and the acceleration.
# The acceleration is calculated as the average acceleration at point n and n+1. This is so the simulator gives the
# correct force to approximate the acceleration.


if __name__ == '__main__':
    (input1, output1) = generate_data_from_time_series('log-02-13-103144.txt', plot=False)

    print(input1[0:5, :])
    print(input1.shape)
    print(output1)
    print(output1.shape)

    np.savetxt("inputs.csv", input1, delimiter=",")
    #  to append to file
    # with open("test.txt", "ab") as f:
    #     f.write(b"\n")
    #     numpy.savetxt(f, a)